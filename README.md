# exercise-warriors

## Requirements
- .NET 7+ SDK
- An IDE such as JetBrains Rider, Visual Studio Community Edition or VSCode

## Instructions
Run the console program with
```
dotnet run
```

Observe the output:
``` 
A warrior Ninja shows up
I attack as a warrior

A warrior Boxer shows up
I attack as a warrior

A warrior Zombie shows up
I attack as a warrior

A warrior Karateka shows up
I attack as a warrior
```

Modify `Program.cs` but without touching the code between `// DO NOT TOUCH THIS` and `// END OF DO NOT TOUCH THIS` comments to allow each warrior to attach in a different way (e.g: to display something different).

The ninja should hit with nunchaku, the boxer should punch, the zombie should bite and the karateka should kick.


